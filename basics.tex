\begin{frame}{Overview}
    \begin{itemize}
        \item \textbf{What is {\LaTeX}?}
        \item \textbf{Installing {\LaTeX}}
        \item \textbf{{\LaTeX} Basics} --- In this section we will cover all of
          the essentials for how to use {\LaTeX}.
        \item \textbf{How to be \LaTeX perts} --- A smattering of cool things
          you can do with {\LaTeX}.
    \end{itemize}

    {%
        \footnotesize
        Note a lot of this presentation was borrowed from Jack's presentation on
        {\LaTeX}: \url{https://github.com/jackrosenthal/lug-latex-presentation}.
    }
\end{frame}

\section{What is {\LaTeX}?}

\begin{frame}{Lay-teck or Lah-teck or Lay-tecks? Settling the question once and
        for all.}
    \only<1>{%
        \begin{block}{From the \TeX book}
            English words like `technology' stem from a Greek root beginning
            with the letters $\tau\epsilon\chi\ldots$; and this same Greek word
            means \textsl{art} as well as technology...

            \medskip
            Insiders pronounce the $\chi$ of \TeX\ as a Greek chi, not as an
            `x', so that \TeX\ rhymes with the word blecchhh... When you say it
            correctly to your computer, the terminal may become slightly moist.
        \end{block}
    }
    \only<2>{%
        \begin{block}{From \LaTeX : A Document Preparation System}
            One of the hardest things about {\LaTeX} is deciding how to
            pronounce it. This is also one of the few things I'm not going to
            tell you about \LaTeX, since pronunciation is best determined by
            usage, not fiat. {\TeX} is usually pronounced teck, making lah-teck,
            and lay-teck the logical choices.
        \end{block}
    }
\end{frame}

\begin{frame}{{\LaTeX} is a \textit{Typesetting} Program}
    \only<1-2>{%
        \begin{itemize}[<+->]
            \item Typesetting is the art of putting \textit{types} on a page.
            \item \textbf{Types} are physical or digital representations of
                letters or symbols.
        \end{itemize}
        \visible<2>{%
            \center
            \includegraphics[width=0.75\textwidth]{./graphics/Metal_movable_type}
            {\tiny\url{https://en.wikipedia.org/wiki/Typesetting\#/media/File:Metal\_movable\_type.jpg}}
        }
    }
    \only<3->{%
        \begin{itemize}
            \visible<3->{
                \item Ligatures appear in professional typesetting, such as in
                    the word find (rather than f{\kern0pt}ind).
            }
            \visible<4->{
                \item Typesetting can involve complex mathematics. {\TeX}
                    handles this \textit{very} well:
                    \begin{align}
                        f : \R \times \R \times \R &\to \R \nonumber \\
                        f(x, y, z) &= \frac{x^2 - y^4 + 81z}{\sqrt{4\left(x^2 + y^2\right)}} \\
                        &= \frac{x^2 - y^4 + 81z}{2\sqrt{x^2 + y^2}}
                    \end{align}
            }
        \end{itemize}
    }
\end{frame}

\begin{frame}{{\LaTeX} vs. Word Typesetting Math}
    \centering
    \textbf{\LaTeX}
    \begin{displaymath}
        -\int_0^{2 \pi} \frac{kQ\, d\theta}{2\pi{\left(a^2 +
        x^2\right)}^{3/2}} (a \sin \theta \,\hat\jmath) = 0
    \end{displaymath}
    \vskip 10pt
    \textbf{Word}\par
    \vskip 5pt
    \includegraphics[height=38pt]{./graphics/wordmath}
\end{frame}

\section{Installing \LaTeX}

\begin{frame}{Linux}
    Most distributions have a package called something like \texttt{texlive}.
    Here are the specifics for some of the more common distributions in LUG:%

    \begin{itemize}
        \item \textbf{Arch Linux:} \texttt{pacman -S texlive-most}
        \item \textbf{Ubuntu/Debian:} \texttt{apt install texlive-full}
        \item \textbf{Fedora:} \texttt{dnf install texlive-scheme-full
                --enablerepo=updates-testing}
    \end{itemize}
\end{frame}

\begin{frame}{Other OSes}
    On macOS/OS X, you can install {\LaTeX} using the MacTex distribution
    (\url{https://www.tug.org/mactex/}).

    On Windows, you can install {\LaTeX} using the MiKTeX distribution
    (\url{https://miktex.org/}). There are other distributions available, but I
    have no idea which is best.
\end{frame}

\begin{frame}{Online Editing}
    When you need to work on group reports concurrently, you have options:

    \vspace{-0.5cm}
    \only<1>{
        \begin{center}
        {\large Overleaf}

        \includegraphics[scale=0.2]{./graphics/OverleafInAction}

        It even comes with Emacs and Vim keybindings, dark theme, etc. Just hit the
        menu and scroll for more options.
        \end{center}
    }
    \only<2>{
        \begin{center}
        {\large CoCalc}

        \includegraphics[scale=0.2]{./graphics/CoCalcInAction}

        {\small CoCalc has a lot more features (like SageMath, Jupyter Notebook,
          RMarkdown, X11 applications, terminals, and better
          tooling).}
        \end{center}
    }
\end{frame}

\begin{frame}{Collaboration}
    The online editors give you real-time collaboration for free.

    If you don't want to use an online editing program, or if you want to use
    your own version control, you can store your {\LaTeX}
    source\footnote[frame]{You will see why it is {\LaTeX} \textit{source}
        soon.} in Git.

    \textit{Note, I recommend that you keep the PDF in source control as well so
        that it is convenient to look at the rendered document in
        GitLab/GitHub.}
\end{frame}

\section{{\LaTeX} Basics}

\begin{frame}{Document Structure}
    To use \LaTeX, you create a source file like \texttt{latex.tex}.

    Write up some code.

    \pause

    Compile your code into a pdf with \texttt{pdflatex latex.tex}.

    Inspect for errors (most commonly missed step).

    \pause

    \begin{block}{Going Off-road}
        Using other features may require using more or different commands. So while
        editors commonly have features to compile \texttt{.tex} code automatically,
        it's not guaranteed.
    \end{block}
\end{frame}


\begin{frame}[fragile]{Document Structure --- Examples}
    For the bare basics of a document:

    \vspace{-0.5cm}
    \begin{columns}[t]
    \begin{column}{0.1\textwidth}

    \end{column}
    \begin{column}{0.9\textwidth}
        \begin{minted}[escapeinside=||]{latex}
        |\tikzmark{T1}\tikzmark{P1}|\documentclass[12pt]{article}
          \usepackage[margin=1cm]{geometry}|\tikzmark{P2}|
          \begin{document}
          |\tikzmark{B1}|Hello World from \LaTeX !
          \begin{equation}
              \sum_{n = 0}^\infty \frac{x^n}{n!} = e^x
          \end{equation}|\tikzmark{B2}|
          \end{document}
        \end{minted}
        \braced{P1}{P2}{Preamble}
        \braced{B1}{B2}{Body}
    \end{column}
    \end{columns}
    \begin{block}{Output}
        Hello World from \LaTeX !
        \begin{equation} \tag{1}
            \sum_{n = 0}^\infty \frac{x^n}{n!} = e^x
        \end{equation}
    \end{block}
\end{frame}

\begin{frame}[fragile]{Document Structure} % Necessary for minted in the frame
    % https://tex.stackexchange.com/questions/430666/fancyverb-error-in-using-minted-package-with-beamer
    More likely:
    \begin{minted}{latex}
        \documentclass[11pt]{amsart}

        \usepackage{graphicx}
        \usepackage{parskip}

        \title{Maze Project--CSCI 406}
        \author{TeX Lion}

        \begin{document}
        \maketitle

        ...
        \end{document}
    \end{minted}

    Many small commands add up to create a clean, nice document.
\end{frame}

\begin{frame}[fragile]{Control Sequences}
    \begin{itemize}[<+->]
        \item Immediately after typing \mintinline{latex}{\ }, \TeX\ expects
            a control word or symbol.
        \item A control word constists of a backslash followed by one or more
            \emph{letters}.
        \item A control symbol constists of a backslash followed by a single
            \emph{nonletter}.
        \item Example: \mintinline{latex}{\input MS} causes \TeX\ to begin
            reading a file called \texttt{MS.tex}.
        \item Example: \TeX\ converts \mintinline{latex}{George P\'olya and Gabor Szeg\"o} to `George P\'olya and Gabor Szeg\"o.'
        \item A space after a control word is ignored, to fix this, escape
            the space after a control word when required.
    \end{itemize}
\end{frame}

\begin{frame}[fragile]{Control Sequences --- Arguments and Options}
    In most cases, you tend to use curly braces to specify arguments to control
    sequences.

    \begin{columns}[t]
    \begin{column}{0.1\textwidth}

    \end{column}
    \begin{column}{0.9\textwidth}
    \begin{minted}{latex}
        \command[options]{argument1}{argument2}
    \end{minted}
    \end{column}
    \end{columns}

    \pause

    \vspace{1cm}

    \begin{block}{Title, Author, Date}
        The commands \mintinline{latex}{\title{Title}} and
        \mintinline{latex}{\author{Sumner}} in the preamble define information that
        \mintinline{latex}{\maketitle} uses to then construct the title section.
    \end{block}
\end{frame}

\begin{frame}[fragile]{Control Sequences --- Changing Fonts of \LaTeX}
    \mintinline{latex}{\textrm{This}} \textrm{This} produces roman typeface
    output.

    \mintinline{latex}{\textsl{This}} \textsl{This} produces slanted typeface
    output.

    \mintinline{latex}{\textit{This}} \textit{This} produces italics typeface
    output.

    \mintinline{latex}{\textbf{This}} \textbf{This} produces bold typeface
    output.

    \mintinline{latex}{\texttt{This}} \texttt{This} produces teletype typeface
    output.

    We can also see how \mintinline{latex}{{}} allows grouping of sections.

    \mintinline{latex}{{\small Text} {\footnotesize getting} {\tiny smaller}}
    {\small Text} {\footnotesize getting} {\tiny smaller}
\end{frame}

\begin{frame}[fragile]{Document Classes --- What are they?}
    Do you remember that line:
    \mintinline{latex}{\documentclass[11pt]{article}}

    \pause

    It specifies the fine details of the formatting somewhere else:

    \begin{itemize}[<+->]
        \item Fonts: types, sizes, and more
        \item Margins and page numbers
        \item Default packages and required presets.
        \item New commands
        \item Arbitrary code
        \item Section numbering
    \end{itemize}

    \pause[\thebeamerpauses]
    You can create your own, which can be really helpful...
\end{frame}

\begin{frame}[fragile]{Document Classes --- 95\% of use cases}
    But luckily there's a lot of presets.
    \begin{multicols}{4}
        \begin{itemize}
            \ttfamily
            \item article
            \item IEEEtran
            \item proc
            \item minimal
            \item report
            \item book
            \item memior
            \item letter
            \item exam
            \item leaflet
            \item beamer
            \item amsart
            \item amsproc
            \item amsbook
            \rmfamily
            \item \dots
        \end{itemize}
    \end{multicols}

    Most journals (such as IEEE, AMS, and ACM) will provide document classes and
    examples (templates) for ``proper'' formatting.

    \pause

    You can also specify options like
    \mintinline{latex}{\documentclass[12pt,a4paper,titlepage,twocolumn]{article}}
\end{frame}

\begin{frame}[fragile]{Environments}
    Environments allow for more advanced grouping and nesting. Like:

    \pause

    \begin{columns}[t]
    \begin{column}{0.45\textwidth}
    \begin{minted}{latex}
        \begin{itemize}
            \item article
            \item IEEEtran
        \end{itemize}
    \end{minted}
    \end{column}
    \begin{column}{0.45\textwidth}
    \begin{itemize}
        \item article
        \item IEEEtran
    \end{itemize}
    \end{column}
    \end{columns}

    \pause
    \begin{center}
        and
    \end{center}
    \vspace{-0.5cm}

    \begin{columns}[t]
    \begin{column}{0.45\textwidth}
    \begin{minted}{latex}
        \begin{enumerate}
            \item article
            \item IEEEtran
        \end{enumerate}
    \end{minted}
    \end{column}
    \begin{column}{0.45\textwidth}
    \begin{enumerate}
        \item article
        \item IEEEtran
        \end{enumerate}
    \end{column}
    \end{columns}
\end{frame}

\begin{frame}[fragile]{Math --- Numbered Equations}
    Use the \mintinline{latex}{equation} environment for numbered equations.
    \begin{minted}[breaklines]{latex}
        \begin{equation}
            \lim_{\Delta x \to 0} \frac{f(x + \Delta x) - f(x)}{\Delta x}
        \end{equation}
    \end{minted}
    \begin{block}{Output}
        \begin{equation} \tag{1}
            \lim_{\Delta x \to 0} \frac{f(x + \Delta x) - f(x)}{\Delta x}
        \end{equation}
    \end{block}
\end{frame}

\begin{frame}[fragile]{Math --- Unnumbered Equations}
    Use the \mintinline{latex}{equation*} environment \footnote[frame]{* usually
    denotes the unnumbered counterpart, e.g. \texttt{section*}} or the more common
    \mintinline{latex}{\[ \]} for unnumbered equations.
    \begin{minted}[breaklines]{latex}
    \[
        \lim_{\Delta x \to 0} \frac{f(x + \Delta x) - f(x)}{\Delta x}
    \]
    \end{minted}
    \begin{block}{Output}
    \[
        \lim_{\Delta x \to 0} \frac{f(x + \Delta x) - f(x)}{\Delta x}
    \]
    \end{block}
\end{frame}

\begin{frame}[fragile]{Math --- Inline Math}
    Use \mintinline{latex}{$ $} or \mintinline{latex}{\( \)} for inline math mode.

    \begin{minted}[breaklines]{latex}
    If $f(x)$ is square integrable,
    then $\lim_{x \to \pm \infty} f(x) = 0$.
    \end{minted}

    \begin{block}{Output}
        If $f(x)$ is square integrable,
        then $\lim_{x \to \pm \infty} f(x) = 0$.
    \end{block}

    There's way too many symbols and commands to cover here. Overleaf has good
    documentation and beginner's guide to guide you from fractions to triple
    integrals.
\end{frame}

\begin{frame}[fragile]{Math --- Display Style Inline}
    You may notice that $\lim$ looks different.

    Inline math mode implicitly uses \mintinline{latex}{\textstyle}. By calling
    \mintinline{latex}{\displaystyle}, you can recover the style inline.

    \pause

    \begin{minted}[breaklines]{latex}
        If $f(x)$ is square integrable,
        then $\displaystyle \lim_{x \to \pm \infty} f(x) = 0$.

        Let $f(x) = \displaystyle \frac{\sin{x}}{x}$.
    \end{minted}

    \pause

    \begin{block}{Output}
        If $f(x)$ is square integrable,
        then $\displaystyle \lim_{x \to \pm \infty} f(x) = 0$.

        Let $f(x) = \displaystyle \frac{\sin{x}}{x}$.
    \end{block}

    \pause

    We'll see some more vital environments later with \mintinline{latex}{amsmath}.
\end{frame}

\begin{frame}[fragile]{Whitespace}
    \begin{itemize}[<+->]
        \item \LaTeX\ treats most forms of whitespace as just a single space.
        \item Paragraphs come from blank lines (or rarely \mintinline{latex}{\par}).
    \end{itemize}

    \pause[\thebeamerpauses]

    Command sequences fine tune whitespace for math and text.

    \begin{block}{In increasing width,}
        \begin{minted}[breaklines]{latex}
            \, \> \; \. \: \space \ ~ \enspace \hphantom{xyz} \quad \qquad  \hfill
        \end{minted}

        \mintinline{latex}{\\} marks a new line, common only in math mode and
        tables.
    \end{block}
\end{frame}

\begin{frame}[fragile]{Special Characters}
    Due to the many reserved characters and lack of default unicode support, many
    characters have are instead invoked with commands.

    \mintinline{latex}{H\"uhnchen} \hfill H\"uhnchen

    \mintinline{latex}{C\'omo est\'as} \hfill C\'omo est\'as

    \begin{alertblock}{``''}
        \mintinline{latex}{"Don't do this"} \hfill "Don't do this"

        \mintinline{latex}{``Do this''} \hfill ``Do this''
    \end{alertblock}
\end{frame}

\begin{frame}{Badness}
    \LaTeX\ uses a rather complicated algorithm for determining how to lay out
    flexible elements in your document.
    \pause

    \begin{itemize}[<+->]
        \item {\LaTeX} has a badness value from 0 to 10,000, where 0 is perfect
            and 10,000 is infinitely bad.
        \item When a line is perfect in spacing between words and no
            hyphenation, the badness will be zero.
        \item As words get too tight or too narrow or too far apart, the badness
            increases.
        \item Hyphenation in words adds a lot of badness!
        \item {\LaTeX} then optimises the badness of each line, trying to get it
            as low as possible.
    \end{itemize}
\end{frame}

\begin{frame}[fragile]{Sections}
    Often (especially in academic papers) you need section breaks. {\LaTeX}
    supports \textit{automatically numbered} sections out-of-the-box.

    \begin{columns}[t]
        \begin{column}{0.45\textwidth}
            \begin{minted}{latex}
                \section{Introduction}
                \lipsum[1]

                \subsection{Related Work}
                \lipsum[1]

                \subsubsection{Cool Stuff}
                \lipsum[1]

                \section{Methods}
                \lipsum[1]

                % Without Numbers
                \section*{No Number}
                \lipsum[1]
            \end{minted}
        \end{column}
        \begin{column}{0.45\textwidth}
            \center
            \vspace{-0.25cm}
            \frameoutput{\includegraphics[width=0.9\textwidth]{examples/sections}}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}[fragile]{Graphics}
    Pictures are handled using the \texttt{graphicx} package.

    First, include the following in your preamble.
    \begin{minted}{latex}
        \usepackage{graphicx}
    \end{minted}
    \pause

    Then, in the body where you want the figure
    \begin{minted}{latex}
        \includegraphics[width=0.5\textwidth]{graphics/tree.jpg}
    \end{minted}

    \begin{block}{Options}
        Note that there are many other options (or you can specify no options),
        and the filename extension is not necessary.
    \end{block}
\end{frame}

\begin{frame}{Graphics --- Output}
    \begin{center}
        \frameoutput{\includegraphics[width=0.55\textwidth]{examples/graphics}}
    \end{center}
\end{frame}

\begin{frame}[fragile]{Tables}
    Tables are created using the \texttt{tabular} environment.

    \vspace{-0.7cm}
    \begin{columns}[t]
        \begin{column}{0.45\textwidth}
            \begin{minted}{latex}
                \begin{tabular}{|l|l|}
                    \hline
                    Program & Review \\
                    \hline
                    \LaTeX & Good \\
                    Microsoft Word & Sucks \\
                    \hline
                \end{tabular}
            \end{minted}
        \end{column}
        \begin{column}{0.45\textwidth}
            \center
            \vspace{0.8cm}
            \begin{tabular}{|l|l|}
                \hline
                Program & Review \\
                \hline
                \LaTeX & Good \\
                Microsoft Word & Sucks \\
                \hline
            \end{tabular}
        \end{column}
    \end{columns}
    \vspace{0.3cm}
    \pause

    % total hack to make it not jump between first and second slide.
    \only<1>{\vspace{2.6cm}}
    \only<2>{
        The first parameter is the table specification. Mainly, it is used to
        specify column alignments.
        \begin{itemize} \small
            \item \texttt{l}, \texttt{r}, \texttt{c} specify left, right, and center
                justification, respectively.
            \item \texttt{|} and \texttt{||} specify single and double vertical
                lines, respectively.
        \end{itemize}
    }
    \only<3>{
        Inside the table body, the following have significance:
        \begin{itemize} \small
            \item \texttt{\&} --- separates columns.
            \item \texttt{\textbackslash\textbackslash} --- creates newline.
            \item \texttt{\textbackslash hline} --- creates horizontal line.
        \end{itemize}
    }
    \only<4>{
        \vspace{0.1cm}
        There are many other options and cool things you can do with tables.
        For example, you can have \textit{paragraph columns} which allow for
        word wrapping.

        Look up the documentation for details.
        \vspace{0.1cm}
    }
\end{frame}

\begin{frame}[fragile]{Automatically Numbered Figures}
    With \LaTeX, you can create \textit{automatically} numbered \textbf{figures}.

    \begin{minted}{latex}
        \begin{figure}
            \centering
            \includegraphics[width=0.2\textwidth]{graphics/tree}
            \caption{This is a tree.}
        \end{figure}
    \end{minted}
    \pause

    \begin{block}{Output}
        \begin{figure}
            \centering
            \only<1>{\vspace{0.2\textwidth}}
            \only<2>{\includegraphics[width=0.2\textwidth]{graphics/tree}}
            \caption{This is a tree.}
        \end{figure}
    \end{block}
\end{frame}

\begin{frame}[fragile]{Automatically Numbered Tables}
    With \LaTeX, you can create \textit{automatically} numbered \textbf{tables}.

    \begin{minted}{latex}
        \begin{table}
            \centering
            \caption{This is a table.}
            \begin{tabular}{|l|l|}
                ...
            \end{tabular}
        \end{table}
    \end{minted}
    \pause

    \begin{block}{Output}
        \begin{table}
            \centering
            \caption{This is a table.}
            \begin{tabular}{|l|l|}
                \hline
                Program & Review \\
                \hline
                \LaTeX & Good \\
                Microsoft Word & Sucks \\
                \hline
            \end{tabular}
        \end{table}
    \end{block}
\end{frame}

\begin{frame}[fragile]{Tables of Contents}
    In addition to automatically numbering sections, tables, and figures,
    {\LaTeX} can automatically generate a Table of Contents, List of Tables,
    List of Figures:
    \begin{minted}{latex}
        \tableofcontents
        \listoffigures
        \listoftables
    \end{minted}
    \pause

    \begin{block}{Note on Unnumbered Sections}
        By default, the Table of Contents will only include \textit{numbered}
        sections. To add an unnumbered section, you have to add this above your
        \texttt{section*}.
        \begin{minted}{latex}
            \addcontentsline{toc}{section}{Unnumbered Section}
            \section*{Unnumbered Section}
        \end{minted}
    \end{block}
\end{frame}

\begin{frame}[fragile]{Tables of Contents --- Output}
    \centering
    \includegraphics[width=\textwidth]{graphics/toc-lof-lot}
    \tiny
    \href{https://texblog.org/2013/04/29/latex-table-of-contents-list-of-figurestables-and-some-customizations}
    {texblog.org/2013/04/29/latex-table-of-contents-list-of-figurestables-and-some-customizations}
\end{frame}

\begin{frame}[fragile]{Cross References}
    To reference another table, figure, section, or equation, you can
    add a \texttt{\textbackslash label} to the element and then elsewhere use
    the \texttt{\textbackslash ref} control sequence to reference it.
    \pause

    \vspace{-0.6cm}
    \begin{columns}[t]
        \begin{column}{0.45\textwidth}
            \begin{minted}{latex}
                \begin{figure}
                    \caption{Cool Thing}
                    \label{fig:cool}
                \end{figure}

                See Figure \ref{fig:cool}
                for details.
            \end{minted}
        \end{column}
        \begin{column}{0.45\textwidth}
            \center
            \begin{figure}
                \caption{Cool Thing}
                \label{fig:cool}
            \end{figure}

            See Figure \ref{fig:cool} for details.
        \end{column}
    \end{columns}
    \vspace{0.3cm}
    \pause

    \only<1-2>{\vspace{2cm}}
    \only<3>{
        \begin{block}{\texttt{\textbackslash label} placement}
            It is important to put the \texttt{\textbackslash label} \textit{after}
            the caption. Don't ask why.
        \end{block}
    }
    \only<4>{
        \begin{block}{This sucks! My reference shows up as ``??''!}
            To fix this, recompile. {\LaTeX} uses an intermediate file to determine
            what figures/tables/sections exist.
        \end{block}
    }
\end{frame}

\begin{frame}{Essential Resources}
    How to learn more?

    \begin{itemize}[<+->]
        \item \st{Official Documentation}
        \item \st{LaTeX Wikibook}
        \item \st{Lists of Symbols}
    \end{itemize}

    \pause[\thebeamerpauses]

    \begin{alertblock}{The True MVP}
        \textbf{Overleaf Documentation}

        \textbf{Detexify}
    \end{alertblock}
\end{frame}

\begin{frame}{Overleaf Documentation}
    \begin{center}
        \includegraphics[scale=0.25]{graphics/OverleafDocumentation}
    \end{center}
\end{frame}

\begin{frame}{Detexify}
    \begin{center}
        \includegraphics[scale=0.35]{graphics/Detexify}
    \end{center}
\end{frame}
