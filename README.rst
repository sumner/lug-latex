LUG LaTeX Presentation
######################

LaTeX Presentation for LUG.

Authors/Presenters
------------------

- `Sumner Evans`_
- `Joseph McKinsey`_

.. _Sumner Evans: https://gitlab.com/sumner
.. _Joseph McKinsey: https://gitlab.com/josephmckinsey

Topics Covered
--------------

- **What is LaTeX?** What is LaTeX, and why you should use it.
- **Installing LaTeX** How to install LaTeX on your system.

  - **Online Collaborative Editing** Overleaf and CoCalc.

- **LaTeX Basics** The bare essentials that you need to know about LaTeX.

  - **Document Structure** How to start a LaTeX document.
  - **Control Sequences** What are all those backslashes?
  - **Document Classes** All of the templates!
  - **Environments** Group stuff together on steroids.
  - **Math** The basics of math typesetting.
  - **Whitespace and Special Characters** How LaTeX deals with whitespace and
    special characters.
  - **Badness** How LaTeX figures out how to justify your text.
  - **Sections** Breaking your paper up.
  - **Graphics** Including graphics in your document.
  - **Tables** Tables
  - **Figures** Automatically numbered!
  - **Tables of Contents** Sections, Tables, Figures
  - **Referencing Things** Tables, Figures, Equations
  - **How to Google Latex Symbols** Detexify! Overleaf documentation.

- **How to be LaTeXperts** A smattering of cool things you can do with LaTeX.

  - **Advanced Math** Things such as ``align`` and ``\left`` and ``\right``.

    - **amsmath, amssymb, amsthm** The golden three. **amsart** for the final
      touch. (Used for aligning, matrices, spacing, dots, modulus, etc.)

  - **Spacing Hacks** When all else fails use ``[h|v]space``.
  - **Presentations in LaTeX!** Like this one. Beamer
  - **Citations** Yes, it can do it for you by the power of **biblatex**.
  - **Procedural Figures** A comprehensive guide on how to pretend to use Tikz.
  - **Useful Packages** Packages that you'll probably be importing 90% of the
    time.

    - **appendix** exactly what it says.
    - **algorithm2e** for having aesthetically pleasing pseudocode.
    - **hyperref** for including links.
    - **minted** for including code into your documents.
    - **parskip** for removing the weird indentation.
    - **pdfpages** for pasting pages of another PDF directly into you're
      document.
    - **siunitx** for having usable unit macros.

  - **How to Define Your Own Commands, Environments, and More** save time.
  - **Defining Your Own Document Classes** Because you need more than
    ``article``.
  - TODO more stuff here!


Other Presentations and Resources
---------------------------------

- `Jack's LaTeX presentation <jack_>`_

.. _jack: https://github.com/jackrosenthal/lug-latex-presentation
