\documentclass{lug}

\usepackage{fontawesome}
\usepackage{etoolbox}
\usepackage{textcomp}
\usepackage[nodisplayskipstretch]{setspace}
\usepackage{xspace}
\usepackage{verbatim}
\usepackage{multicol}
\usepackage{soul}

\usepackage{amsmath,amssymb,amsthm}

\usepackage[linesnumbered,commentsnumbered,ruled,vlined]{algorithm2e}
\newcommand\mycommfont[1]{\footnotesize\ttfamily\textcolor{blue}{#1}}
\SetCommentSty{mycommfont}
\SetKwComment{tcc}{ \# }{}
\SetKwComment{tcp}{ \# }{}

\usepackage{siunitx}

\usepackage{tikz}
\usepackage{pgfplots}
\usetikzlibrary{decorations.pathreplacing,calc,arrows.meta,shapes,graphs}

\AtBeginEnvironment{minted}{\singlespacing\fontsize{10}{10}\selectfont}
\usefonttheme{serif}

\makeatletter
\patchcmd{\beamer@sectionintoc}{\vskip1.5em}{\vskip0.5em}{}{}
\makeatother

% Math stuffs
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\lcm}{\text{lcm}}
\newcommand{\Inn}{\text{Inn}}
\newcommand{\Aut}{\text{Aut}}
\newcommand{\Ker}{\text{Ker}\ }
\newcommand{\la}{\langle}
\newcommand{\ra}{\rangle}

\newcommand{\yournewcommand}[2]{Something #1, and #2}

\newenvironment{question}[1]{\par\textbf{Question #1.}\par}{}

\newcommand{\pmidg}[1]{\parbox{\widthof{#1}}{#1}}
\newcommand{\splitslide}[4]{
    \noindent
    \begin{minipage}{#1 \textwidth - #2 }
        #3
    \end{minipage}%
    \hspace{ \dimexpr #2 * 2 \relax }%
    \begin{minipage}{\textwidth - #1 \textwidth - #2 }
        #4
    \end{minipage}
}

\newcommand{\frameoutput}[1]{\frame{\colorbox{white}{#1}}}

\newcommand{\tikzmark}[1]{%
\tikz[baseline=-0.55ex,overlay,remember picture] \node[inner sep=0pt,] (#1)
{\vphantom{T}};
}

\newcommand{\braced}[3]{%
    \begin{tikzpicture}[overlay,remember picture]
        \draw [thick,decorate,decoration={brace,raise=1ex,amplitude=4pt},blue] (#2.south west-|T1.south west) -- node[anchor=west,left,xshift=-1.8ex,text=olive]{#3} (#1.north west-|T1.south west);
    \end{tikzpicture}
}

\title{\LaTeX}
\author{Sumner Evans and Joseph McKinsey}
\institute{Mines Linux Users Group}

\begin{document}

\include{basics}

\section{How to be \LaTeX perts}

\begin{frame}[fragile]{Advanced Math}
    \LaTeX\ has a lot of odd holes, so the AMS gave us:

    \pause

    \vspace{1cm}

    \begin{block}{The Trifecta}
        \texttt{amsmath} \hfill \texttt{amssymb}

        \begin{center}
            \texttt{amsthm}
        \end{center}

        A trio so inseparable, no one is sure if they should ever import just
        one.

    \end{block}

\end{frame}

\begin{frame}[fragile]{Advanced Math --- Matrices}

    Tables are annoying and uncomfortable: so \texttt{amsmath} provides many matrix
    environments:

    \begin{columns}[t]
    \begin{column}{0.45\textwidth}
        \begin{minted}{latex}
            \[
                \begin{bmatrix}
                    0 & 2 & 3 & 5 \\
                    1 & 0 & 6 & 3 \\
                    2 & 2 & 0 & 2 \\
                    4 & 9 & 3 & 0
                \end{bmatrix}
                \begin{pmatrix}
                    1 & 2 \\
                    3 & 4
                \end{pmatrix}
            \]
        \end{minted}
    \end{column}

    \begin{column}{0.45\textwidth}
        \vspace{1.5cm}
        \[
            \begin{bmatrix}
                0 & 2 & 3 & 5 \\
                1 & 0 & 6 & 3 \\
                2 & 2 & 0 & 2 \\
                4 & 9 & 3 & 0
            \end{bmatrix}
            \begin{pmatrix}
                1 & 2 \\
                3 & 4
            \end{pmatrix}
        \]
    \end{column}
    \end{columns}
\end{frame}

\begin{frame}[fragile]{Advanced Math --- Usable Multiline Environments}
    Don't even bother trying to use \mintinline{latex}{eqnarray}.

    \begin{minted}[breaklines]{latex}
        \begin{align*}
            \frac{\partial y}{\partial x} &= \frac{\partial}{\partial x} \left( x \log{z} - 3 w \right) \\
                                      &= \log{z} \in \mathbb{R}
        \end{align*}
    \end{minted}
    \footnote[frame]{Here, I also use the resizable \mintinline{latex}{\left(
        \right)} and blackboard symbols \mintinline{latex}{\mathbb{}}}

    \begin{block}{Output}
        \begin{align*}
            \frac{\partial y}{\partial x} &= \frac{\partial}{\partial x} \left( x \log{z} - 3 w \right) \\
                                          &= \log{z} \in \mathbb{R}
        \end{align*}
    \end{block}

\end{frame}

\begin{frame}{Advanced Math --- And More...}
    \begin{multicols}{2}

    \begin{itemize}
    \item Better Equation Numbering
    \item Oversets and undersets
    \item Underbraces and overbraces
    \item \mintinline{latex}{\text} command
    \item Better labeled arrows
    \item Cases environment
    \item Math-mode Page Breaks
    \item Boxed equations
    \item Custom operators
    \item Substacks
    \item short inter text \footnote[frame]{Actually part of the
      \texttt{mathtools} package, which is a less common extension of
      \texttt{amsmath}}
    \end{itemize}

    \end{multicols}
\end{frame}

\begin{frame}[fragile]{Advanced Spacing Hackery}
    Normally, {\LaTeX} is great at automatically spacing things for you. But
    occasionally it just fails. In these cases, you have two options:
    \pause
    \begin{enumerate}
        \item Try and manipulate the options to get it to do what you want.
            \pause
        \item Use \texttt{\textbackslash hspace} and
            \texttt{\textbackslash vspace}.
    \end{enumerate}
    \pause
    \begin{block}{Usage}
        \begin{center}
            \mintinline{latex}{\hspace{<dimension>}} or
            \mintinline{latex}{\vspace{<dimension>}}
        \end{center}
        Note that \texttt{<dimension>} can be negative. Useful when
        there's too much padding and you have no idea how to fix it.
    \end{block}
\end{frame}

\begin{frame}{Beamer --- Presentations in {\LaTeX}!}
    One of the great things about {\LaTeX} is that you can use it to make
    presentations. You can get all of the benefits of {\LaTeX} in your
    presentations!
    \pause

    To make a presentation in {\LaTeX}, use the \texttt{beamer} document class.
    \pause

    All of your knowledge of how to use {\LaTeX} for documents applies.
    \pause
    \begin{itemize}[<+->]
        \item You can use \texttt{section}s to make divisions in your
            presentation. (Note that \texttt{[sub]+sections} are ignored.)
        \item You have to put everything into \texttt{frame} environments.
        \item To make a title slide for your presentation, create a slide with
            just \texttt{\textbackslash maketitle} inside.
    \end{itemize}
\end{frame}

\begin{frame}[fragile]{Beamer --- Example}
    Here's a basic example Beamer presentation.
    \inputminted{latex}{examples/beamer.tex}
\end{frame}

\begin{frame}{Beamer --- Rendered Result}
    \includegraphics[width=0.49\textwidth]{examples/beamer}
    \includegraphics[width=0.49\textwidth,page=2]{examples/beamer}
\end{frame}

\begin{frame}[fragile]{Beamer --- Themes}
    By default, Beamer is pretty ugly. You can select from a multitude of
    themes, or even spin your own.
    \pause

    This presentation uses a modified version of the \texttt{metropolis} theme.
    Jack Rosenthal created this version of the theme for LUG.
    \pause

    Other options include \texttt{Luebeck}, \texttt{Rochester}, and
    \texttt{Pittsburgh}.
    \pause

    To set the theme, just put
    \begin{minted}{latex}
        \usetheme{Luebeck} % replace with your preferred theme
    \end{minted}
    after all of the \texttt{\textbackslash usepackage} lines.
\end{frame}

\begin{frame}[fragile]{Beamer --- Pause}
    We showed an example of \texttt{\textbackslash pause} earlier, and we showed
    that it allows you to hide parts of the slide.
    \pause

    However, it can be tedious to add \texttt{\textbackslash pause}s after each
    element in a list. To remedy this, there is a cool shorthand:

    \begin{minted}{latex}
        \begin{itemize}[<+->]
            \item Foo
            \item Bar
        \end{itemize}
    \end{minted}
    \pause

    \textbf{Result:}
    \pause
    \begin{itemize}[<+->]
        \item Foo
        \item Bar
    \end{itemize}
\end{frame}

\begin{frame}{Citations}
    One of the coolest things that you can do with {\LaTeX} is have
    automatically generated citations.
    \pause

    Unfortunately, there are approximately 1000 different ways of doing this,
    and they are not entirely compatible with one another.

    However, when it works correctly, it's amazing.
\end{frame}

\begin{frame}[fragile]{Citations --- BibTeX}
    The general idea is that you store citations in a machine-readable format
    called \textit{BibTeX}.
    \pause

    Normally, you put these citations in a \texttt{.bib} file. Each entry in the
    file looks something like this:
    \begin{minted}{bibtex}
        @presentation{Evans-McKinsey:LaTeX:LUG,
          author        = "Evans, Sumner and McKinsey, Joseph",
          title         = "\LaTeX",
          publisher     = "Mines LUG",
          address       = "Golden, CO",
          year          = {2018}
          url           = {https://gitlab.com/sumner/lug-latex
        },
    \end{minted}
    \pause

    There are many different types of entries. This website has a more
    comprehensive list of the entry types:
    \url{https://libguides.nps.edu/citation/ieeebibtex}.
\end{frame}

\begin{frame}{Citations --- Pro Tip}
    Many academic websites allow you to download citations as \texttt{.bib}
    files. For example, this is the interface for IEEE Xplore Digital Library:
    \begin{center}
        \includegraphics[width=0.9\textwidth]{./graphics/ieee-xplore-bibtex-download}
    \end{center}
\end{frame}

\begin{frame}[fragile]{Citations --- Usage in Documents}
    Once you have the citation in your \texttt{.bib} file, you can cite within
    your document like so:
    \begin{minted}{latex}
        To be, or not to be \cite{Shakespeare:Hamlet}.
    \end{minted}
    \pause

    Then, where you want the actual bibliography in your document, add this
    line to \textbf{automagically} generate your bibliography:
    \begin{minted}{latex}
        \bibliography{path/to/references.bib}
    \end{minted}

    \pause
    \begin{block}{A note on options}
        \footnotesize
        There are a multitude of options which you can configure (e.g. citation
        and bibliography styles). There are also many different types of
        \texttt{\textbackslash cite} variants which allow you to style the
        citations further.
    \end{block}
\end{frame}

\begin{frame}[fragile]{Citations --- Compilation}
    When you compile a document and want to use BibTeX, you need to do a
    sequence something like:
    \begin{minted}{sh}
        xelatex your-file.tex
        bibtex your-file
        xelatex your-file.tex
        xelatex your-file.tex
    \end{minted}
    \pause

    The reason for this is that BibTeX uses the \texttt{.aux} files to figure
    out what citations you need. \pause Nobody understands why you need to have
    two compilations after calling \texttt{bibtex} though\dots
    \footnote[frame]{something something partial \texttt{.aux} compilation}
\end{frame}

\begin{frame}{Citations --- Pitfalls}
    \begin{itemize}[<+->]
    \item Getting group members to use BibTeX for their citations.
    \item Adhering to strict nonstandard bibliography formats.
    \item Formatting non-ascii is more difficult.
    \item Harder to justify the effort with less than 4 entries.
    \item May not play nice with your page setup.
    \end{itemize}
\end{frame}

\begin{frame}[fragile]{Procedural Figures}
    You can use the \texttt{tikz} library to do generate figures in {\LaTeX}.
    Figures you can create include charts, plots, graphs, trees, and other types
    of figures.

    In general, just look up what you are trying to do for examples.
    \pause

    Examples:
    \begin{columns}[t]
    \begin{column}{0.5\textwidth}
        \center
        \resizebox {0.9\textwidth} {!} {
            \begin{tikzpicture}
                \begin{axis}[axis lines = left]
                    \addplot [domain=-10:10, samples=200, color=red, very thick]
                    {(1 / 2) * exp(-abs(x))};
                    \addlegendentry{$\mu=0$, $b=1$}
                    \addplot [domain=-10:10, samples=200, color=blue, very thick]
                    {(1 / (2 * 2)) * exp(-abs(x) / 2)};
                    \addlegendentry{$\mu=0$, $b=2$}
                    \addplot [domain=-10:10, samples=200, color=green, very thick]
                    {(1 / (2 * 2)) * exp(-abs(x - 2) / 2)};
                    \addlegendentry{$\mu=2$, $b=2$}
                \end{axis}
            \end{tikzpicture}
        }
    \end{column}
    \pause
    \begin{column}{0.5\textwidth}
        \center
        \resizebox {0.65\textwidth} {!} {
            \begin{tikzpicture}
                [->,>=Stealth,auto=left,every node/.style={circle}]
                \node[fill=red!50] (ab) at (4,10) {$\overline{AB}$};
                \node[fill=red!50] (ba) at (5.2,10) {$\overline{BA}$};
                \node[fill=blue!30] (be) at (1,7) {$\overline{BE}$};
                \node[fill=blue!30] (eb) at (1,5.8) {$\overline{EB}$};
                \node[fill=red!50] (bf) at (4,5) {$\overline{BF}$};
                \node[fill=red!50] (fb) at (5.2,5) {$\overline{FB}$};
                \node[fill=green!30] (ef) at (1,4) {$\overline{EF}$};
                \node[fill=green!30] (fe) at (1,2.8) {$\overline{FE}$};
                \node[fill=red!50] (fk) at (4,0) {$\overline{FK}$};
                \node[fill=red!50] (kf) at (5.2,0) {$\overline{KF}$};
                \node[fill=blue!30] (bc) at (7,7) {$\overline{BC}$};
                \node[fill=blue!30] (cb) at (7,5.8) {$\overline{CB}$};
                \node[fill=green!30] (fg) at (7,4) {$\overline{FG}$};
                \node[fill=green!30] (gf) at (7,2.8) {$\overline{GF}$};
                \node[fill=green!30] (cd) at (10,9) {$\overline{CD}$};
                \node[fill=green!30] (dc) at (10,7.8) {$\overline{DC}$};
                \node[fill=red!50] (cg) at (10,5) {$\overline{CG}$};
                \node[fill=red!50] (gc) at (10,3.8) {$\overline{GC}$};

                \foreach \from/\to in
                {ab/be,ab/bf,eb/ba,eb/bc,ef/fk,ef/fg,bf/fk,kf/fb,kf/fe,kf/fg,gf/fk,fb/ba,cb/be,dc/cb,bc/cd}
                \draw (\from) -- (\to);

            \end{tikzpicture}
        }
    \end{column}
    \end{columns}
\end{frame}

\begin{frame}[fragile]{Procedural Figures --- General Usage}
    To create \texttt{tikz} pictures, first add the following lines to the
    preamble:
    \begin{minted}{latex}
        \usepackage{tikz}
        \usepackage{pgfplots} % sometimes not necessary
        \usetikzlibrary{calc,arrows.meta,shapes,graphs}
            % put whatever tikz libraries you want to use here
    \end{minted}
    \pause

    Then, in general, you will write all of your \texttt{tikz} code in a
    \texttt{tikzpicture} environment:
    \begin{minted}{latex}
        \begin{tikzpicture}
            % tikz code goes here
        \end{tikzpicture}
    \end{minted}
    \pause

    There's a lot that could be covered, but really, just copy and paste from
    the internet until it works.
\end{frame}

\begin{frame}{Procedural Figures --- Alternative Options}
    \begin{itemize}
        \item \textbf{Graphs} --- Graphviz. Uses the \texttt{.dot} format, and
            automatically lays out your graphs for you.\footnote[frame]{Unless
                you are doing something crazy like trying to graph 10,000 nodes
                in a dense social network\dots}
        \item \textbf{Plots} --- Python's matplotlib. Do all your plotting using
            Python, rather than {\LaTeX}.
    \end{itemize}
\end{frame}

\subsection{Useful Packages}

\begin{frame}[fragile]{Useful Packages --- \mintinline{latex}{appendix}}

    Instead of just using \mintinline{latex}{\appendix} and hoping for the best,
    the appendix package allows options and environments.

    \begin{minted}{latex}
        \usepackage[toc,page]{appendix}
        ...
        \begin{appendices}
        ...
        \end{appendices}
    \end{minted}

    It also supports appendix titles and more.
\end{frame}

\begin{frame}[fragile]{Useful Packages --- \mintinline{latex}{algorithm2e}}
    Most customizable and handsome pseudocode package created.

    \pause

    The hardest part is customizing it to look good in the preamble:

    \begin{block}{Example Preamble}
        \begin{minted}[breaklines]{latex}
            \usepackage[linesnumbered,commentsnumbered,
                        ruled,vlined]{algorithm2e}
            \newcommand\mycommfont[1]{
            \footnotesize\ttfamily\textcolor{blue}{#1}}
            \SetCommentSty{mycommfont}
            \SetKwComment{tcc}{ \# }{}
            \SetKwComment{tcp}{ \# }{}
            ...
        \end{minted}
    \end{block}
\end{frame}

\begin{frame}[fragile]{Useful Packages --- \mintinline{latex}{algorithm2e}}

    \begin{minted}[breaklines]{latex}
        ...
        \begin{algorithm}[H]
            \DontPrintSemicolon
            \KwIn{\(b_w, b_l, b_d\) and \(w_w, w_l, w_d\), \(score, games\)}
            \KwOut{Probability of scoring \(score\) or better.}
            Initialize \((games \times score)\) array called \(dp\) to 2s\;
            \For{\(g \in (0, \ldots, games)\)}{
                \tcp{This is a full line comment}
                \eIf{\(310\)}{
                    Do x and y. \tcc{a little comment.}
                }{
                    Do z.
                }
                \tcc{This is a comment}
            }
            \Return \(dp[games][score]\)\;
            \caption{DP Probability Algorithm}
        \end{algorithm}
    \end{minted}
\end{frame}

\begin{frame}{Useful Packages --- \mintinline{latex}{algortihm2e} Output}

    \begin{algorithm}[H]
        \DontPrintSemicolon
        \KwIn{\(b_w, b_d\) and \(w_w, w_d\), \(score, games\)}
        \KwOut{Probability of scoring \(score\) or better.}
        Initialize \((games \times score)\) array \(dp\) to 2s\;
        \For{\(g \in (0, \ldots, games)\)}{
            \tcp{This is a full line comment}
            \eIf{\(310\)}{
                Do x and y. \tcc{a little comment.}
            }{
                Do z.
            }
            \tcc{This is a comment}
        }
        \Return \(dp[games][score]\)\;
        \caption{DP Probability Algorithm}
    \end{algorithm}
\end{frame}

\begin{frame}[fragile]{Useful Packages --- \mintinline{latex}{hyperref}}
    You can link within a document and to the World Wide Web.\footnote[frame]{It
        also automatically from \texttt{ref}s to \texttt{label}s.}

    \begin{minted}{latex}
        \url{https://google.com} or \href{https://google.com}{this}.
    \end{minted}

    \begin{block}{Output}
        \url{https://google.com} or \href{https://google.com}{this}.
    \end{block}
\end{frame}


\begin{frame}[fragile]{Useful Packages --- \mintinline{latex}{minted}}

    The cleanest code highlighting in the west: \footnote[frame]{Sadly, you can't
        nest minted environments.}

    \begin{verbatim}
        \begin{minted}{python}
            def hello():
                print("hello")
        \end{minted}
    \end{verbatim}

    \begin{block}{Output}
        \begin{minted}{python}
            def hello():
                print("hello")
        \end{minted}
    \end{block}
\end{frame}


\begin{frame}[fragile]{Useful Packages --- \mintinline{latex}{parskip}}

    \begin{center}
        \mintinline{latex}{\usepackage{parskip}}
    \end{center}

    \[
        \Downarrow
    \]

    \vspace{0.5cm}

    \begin{center}
        No paragraph indentation. Just empty lines.
    \end{center}
\end{frame}


\begin{frame}[fragile]{Useful Packages --- \mintinline{latex}{pdfpages}}

    Pastes other \texttt{.pdf}s directly into your pdf.\footnote[frame]{Except
        in Beamer. If you try to do this in a Beamer presentation is blows up
        for some reason. Just use \mintinline{latex}{\includegraphics}.}

    \begin{minted}{latex}
        \usepackage[final]{pdfpages}
        ...
        \includepdf[pages=-]{file.pdf}
    \end{minted}
\end{frame}


\begin{frame}[fragile]{Useful Packages --- \mintinline{latex}{siunitx}}
    For those poor souls who have to care about units in their math, there is a
    better way than \mintinline{latex}{\text{}}.

    \pause

    \begin{minted}{latex}
        \[
            x = \SI{500}{\cubic\metre\per\hour}
              \neq \SI{1023}{\m^3\per\hour}
        \]
    \end{minted}

    \pause

    \begin{block}{Output}
        \[
            x = \SI{500}{\cubic\metre\per\hour}
              \neq \SI{1023}{\m^3\per\hour}
        \]
    \end{block}

    \pause

    You can easily define your own units and shortcuts.
\end{frame}

\begin{frame}[fragile]{Defining Your Own Commands}
    \LaTeX{} can be a very lengthy, so one option to mitigate that is to define
    your own commands in the preamble:\footnote[frame]{Use
      \mintinline{latex}{\renewcommand} to redefine something.}

    \begin{block}{The \mintinline{latex}{\newcommand} command}
        \begin{minted}{latex}
            \newcommand{\yournewcommand}[2]{Something #1, and #2}
        \end{minted}
    \end{block}

    \pause

    \begin{block}{Output}
        \mintinline{latex}{\yournewcommand{test}{this}} $\Rightarrow$
        \yournewcommand{test}{this}
    \end{block}
\end{frame}

\begin{frame}[fragile]{Defining Your Own Commands --- A Real Small Example}
    \begin{minted}{latex}
        \newcommand{\Z}{\mathbb{Z}}
    \end{minted}

    \pause

    \begin{exampleblock}{Small Example}
        \mintinline{latex}{Let $x \in \Z$.}

        Let $x \in \Z$
    \end{exampleblock}
\end{frame}

\begin{frame}[fragile]{Defining Your Own Commands --- A Real Big Example}
    \begin{minted}[breaklines]{latex}
        \newcommand{\tikzmark}[1]{%
            \tikz[baseline=-0.55ex,overlay,remember picture] \node[inner sep=0pt,] (#1)
            {\vphantom{T}};
        }

        \newcommand{\braced}[3]{%
            \begin{tikzpicture}[overlay,remember picture]
                \draw [thick,decorate,decoration={brace,raise=1ex,amplitude=4pt},blue] (#2.south west-|T1.south west) -- node[anchor=west,left,xshift=-1.8ex,text=olive]{#3} (#1.north west-|T1.south west);
            \end{tikzpicture}
        }
    \end{minted}
\end{frame}


\begin{frame}[fragile]{Defining Your Own Commands --- A Real Big Example}
    \begin{minted}[breaklines]{latex}
        \begin{center}
        \tikzmark{E1}
        This could be any thing with nice boundaries.

        Hopefully with multiple lines.
        \tikzmark{E2}
        \end{center}
        \braced{E1}{E2}{Sentence}
    \end{minted}


    \begin{center}
    \tikzmark{E1}
    This could be any thing with nice boundaries.

    Hopefully with multiple lines.
    \tikzmark{E2}
    \end{center}
    \braced{E1}{E2}{Sentence}
\end{frame}

\begin{frame}[fragile]{Defining Your Own Environments}
    Let's say you need a new environment, maybe for exam questions or proofs. You
    can define a completely new environment with
    \footnote[frame]{\mintinline{latex}{\renewenvironment} redefines an environment.
    Square brackets can define arguments.}


    \begin{minted}{latex}
        \newenvironment{<name>}{<begin code>}{<end code>}
    \end{minted}
\end{frame}

\begin{frame}[fragile]{Defining Your Own Environments --- Example}
    \begin{minted}{latex}
        \newenvironment{question}[1]{\par\textbf{Question #1.}\par}{}
    \end{minted}

    \pause

    \begin{exampleblock}{Small Example}
        \begin{minted}{latex}
            \begin{question}{2-3}
                This could be where you put your answer.
            \end{question}
        \end{minted}
    \end{exampleblock}

    \pause

    \begin{block}{Output}
        \begin{question}{2-3}
            This could be where you put your answer.
        \end{question}
    \end{block}
\end{frame}

\begin{frame}{Defining Your Own Theorems --- A short note}
    With the \texttt{amsthm} package, you can define your own theorems very easily
    with some basic \mintinline{latex}{theoremstyle}s. Notable features include:

    \begin{itemize}[<+->]
    \item Numbered Environments.
    \item Quickly creating several simple environments: remarks, lemmas, etc.
    \item Built in styles.
    \end{itemize}
\end{frame}

\begin{frame}{Defining Your Own Document Classes/Packages}
    You probably have a lot of boiler plate in your preamble. You have two options:

    \begin{itemize}[<+->]
    \item Create a package (\texttt{.sty}): your commands can be used anywhere.
    \item Create a document class (\texttt{.cls}): they are part of a type of
    document.
    \end{itemize}

    \pause[\thebeamerpauses]

    Both options force you to use a greater subset of \LaTeX{} commands.

    \pause

    I will focus on creating a document class, since college tends to have a lot of
    boilerplate for assignments. You can find my own document class at
    \url{https://github.com/josephmckinsey/hw-document-class}.
\end{frame}

\begin{frame}[fragile]{Defining Your Own Document Classes}
    You have a section for identification:
    \begin{minted}{latex}
        \NeedsTeXFormat{LaTeX2e}
        \ProvidesClass{<class-name>}[<date> <other information>]
    \end{minted}

    \pause

    Then you can import your own packages with:\footnote[frame]{You can also pass
    along options with \mintinline{latex}{\RequirePackageWithOptions{Package}}}

    \begin{minted}{latex}
        \RequirePackage[<options>]{<package>}[<date>]
    \end{minted}

    \pause

    And classes with:

    \begin{minted}{latex}
        \LoadClass[<options>]{<package>}[<date>]
    \end{minted}
\end{frame}

\begin{frame}[fragile]{Defining Your Own Document Classes -- Example}
    The \texttt{lug.cls} document class:
    \small
    \begin{minted}{latex}
        \NeedsTeXFormat{LaTeX2e}
        \ProvidesClass{lug}[2016/08/31]

        \LoadClass{beamer}

        \usetheme[numbering=none,
                 progressbar=frametitle,
                 block=fill]{metropolis}
        \setbeamercovered{dynamic}
        \RequirePackage{graphicx}

        \RequirePackage{ifxetex}
        \ifxetex\RequirePackage{fontspec}\fi

        \RequirePackage{minted}
        \RequirePackage{xcolor}
        \RequirePackage{hyperref}
        ...
    \end{minted}
\end{frame}

\begin{frame}
    \Huge
    Questions?
\end{frame}

\begin{frame}{Resources}
    \begin{itemize}
        \item For examples of presentations in {\LaTeX}, just go to the LUG
            website and you can see the source from all of the previous
            presentations for inspiration.
        \item \texttt{clsguide.pdf} is a great 30-pg intro to document
            classes and package writing.
        \item Overleaf
        \item Detexify
        \item LaTex Wikibook
    \end{itemize}
\end{frame}

\end{document}
% Local Variables:
% TeX-command-extra-options: "-shell-escape"
% End:
